from django.db import models
from django.utils import timezone
import datetime


# # my classes
class CarInfor(models.Model):
  # car_id = models.IntegerField(default=0)    
  car_name = models.CharField(max_length=200) 
  price = models.FloatField(default=0)       
  register_time = models.CharField(max_length=200)
  register_location = models.CharField(max_length=200)
  mileage = models.FloatField(default=0)       
  gear_box = models.CharField(max_length=200)
  owned = models.IntegerField(default=0)
  manufacturer = models.CharField(max_length=200)
  grade = models.CharField(max_length=200)
  size_x = models.FloatField(default=0)       
  size_y = models.FloatField(default=0)       
  size_z = models.FloatField(default=0)       
  wheelbase = models.FloatField(default=0)       
  weight = models.FloatField(default=0)       
  displacement = models.FloatField(default=0)       
  cylinder = models.CharField(max_length=200)
  horsepower = models.FloatField(default=0)       
  torque = models.FloatField(default=0)       
  fueltype = models.CharField(max_length=200)
  intake_form = models.CharField(max_length=200)
  oil_type = models.CharField(max_length=200)
  fuel_supply = models.CharField(max_length=200)
  emission_standard = models.CharField(max_length=200)
