/**
 * determine what kinds of graphs can be plotted for given data
 */

/**
 * generate a random scaling...
 */
var randomScalingFactor = function () {
  return Math.round(Math.random() * 100)
}

/**
 * generate random data..
 */
function generateData() {
  var data = []
  for (var i = 0; i < 30; i++) {
    data.push({
      x: randomScalingFactor(),
      y: randomScalingFactor()
    })
  }
  return data
}

/**
 * merge arrays for scatter chart
 * @param {Array[]} xs 
 * @param {Array[]} ys 
 */
function zip_scatter_data(xs, ys) {
  return xs.map(function(v,i) {
    return { x: v, y: ys[i] };
  });
}

/**
 * convert string array to index array
 * @param {Array} op_data array of string data
 */
function str_to_id(op_data) {
  var mappings = new Map();
  var inverse_mappings = new Map();
  var counter = 0;
  var counter_range = [];
  var labels = [];
  for (var v of op_data)
    if (!mappings.has(v)) {
      counter_range.push(counter);
      mappings.set(v, counter);
      inverse_mappings.set(counter, v);
      labels.push(v);
      counter += 1;
    }
  var totals = new Array(counter);
  var op_data_mapped = op_data.map(function(v,i) {
    var mv = mappings.get(v);
    if (!totals[mv]) totals[mv] = 1;
    else totals[mv] += 1;
    return mv;
  });
  return {
    ns: gen_range_n(labels.length),
    data: op_data_mapped, // mapped data
    labels: labels, // all unique strings
    totals: totals, // count of each unique string, in order of being first discovered
  };
}

/**
 * convert number to binned index array
 * @param {Array[number]} xs 
 */
function number_to_id(xs) {
  var max = maximum(xs);
  var min = minimum(xs);
  var diff = Math.abs(max-min);
  var num_bins;
  if (diff <= 1000) num_bins = 20;
  else if (diff <= 5000) num_bins = 35;
  else num_bins = 50;
  var bin_size = diff / num_bins;
  var bins = gen_zeros_n(num_bins);
  var data_mapped = gen_zeros_n(xs.length);
  xs.forEach(function(x,idx) {
    for (var i=1; i<=num_bins; i++)
      if (x <= min+bin_size*i) {
        bins[i-1] += 1;
        data_mapped[idx] = i-1;
        break;
      }
  });
  var labels = gen_range_n(num_bins).map(function(i,_){
    var low = Math.floor(min+i*bin_size);
    var high = Math.floor(min+(i+1)*bin_size);
    return low + "~" + high;
  });
  return {
    ns: gen_range_n(num_bins),
    data: data_mapped,
    labels: labels,
    totals: bins,
  };
}

/**
 * generate tables for stacked histogram
 * depend on str_to_id's representation
 * @param {Array[string]} xstrs 
 * @param {Array[string]} ystrs 
 */
function cross(xstrs, ystrs) {
  var uniques_x = str_to_id(xstrs);
  var uniques_y = str_to_id(ystrs);
  var result = new Array();
  for (var y of uniques_y.ns) {
    result.push(gen_zeros(uniques_x.ns));
  }
  for (var y of uniques_y.ns) {
    uniques_x.data.forEach(function(x,i) {
      if (uniques_y.data[i] == y)
        result[y][x] += 1;
    });
  }
  return {
    datas: result,
    data_labels: uniques_x.labels,
    group_labels: uniques_y.labels,
  };
}

/**
 * cross number with string, producing statistics of each class
 * @param {Array[string]} xstrs 
 * @param {Array[number]} ys 
 */
function crosn(xstrs, ys) {
  var critical_points = {
    maximum: [],
    upper_quantile: [],
    medium: [],
    lower_quantile: [],
    minimum: [],
    class_size: 0,
    labels: null,
    classes: null,
  };
  var uniques_x = str_to_id(xstrs);
  var per_class = gen_arrays(uniques_x.ns);
  critical_points.class_size = uniques_x.ns.length;
  critical_points.labels = uniques_x.labels;
  ys.forEach(function(y,i) {
    per_class[uniques_x.data[i]].push(y);
  });
  per_class.forEach(function(c, i) {
    c.sort(function(a,b){return a>b;});
    critical_points.maximum.push(c[c.length-1]);
    critical_points.upper_quantile.push(c[Math.floor(c.length*3/4)]);
    critical_points.medium.push(c[Math.floor(c.length/2)]);
    critical_points.lower_quantile.push(c[Math.floor(c.length/4)]);
    critical_points.minimum.push(c[0]);
  });
  critical_points.classes = per_class;
  return critical_points;
}

/**
 * make scatter chart object
 * @param {Array[{x:number, y:number}]} data 
 */
function make_scatter_chart(data, label_x, label_y) {
  return {
    type: 'scatter',
    data: {
      datasets: [{
        data: data,
        borderColor: random_border_color(),
        backgroundColor: random_fill_color(),
      }]
    },
    options: {
      legend: {
        display: false,
      },
      scales: {
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: label_x
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: label_y,
          },
        }]
      }
    }
  };
}

/**
 * make line chart object
 * @param {Array[number]} data 
 * @param {Array[string]} data_labels 
 */
function make_line_chart(data, data_labels, legend) {
  if (!data_labels) data_labels = gen_labels(data);
  var sorted = data.map(function(v,i) { return [v,data_labels[i]]; });
  sorted.sort(function(a,b) { return a[0]<b[0]; });
  data = sorted.map(function(v,i) { return v[0]; });
  data_labels = sorted.map(function(v,i) { return v[1]; });
  return {
    type: 'line',
    data: {
      labels: data_labels,
      datasets: [{
        data: data,
        label: legend,
        borderColor: random_fill_color(),
        backgroundColor: random_fill_color(),
        // fill: false,
        // borderWidth: 6,
        pointRadius: 0,
        borderDash: [10,5],
      }]
    }
  };
}

/**
 * make histogram from bins
 * @param {totals: Array[number], labels: Array[string]} bins 
 * @param {string} legend 
 */
function make_histogram(bins, legend) {
  return {
    type: 'bar',
    data: {
      labels: bins.labels,
      datasets:[{
        label: legend,
        borderColor: random_border_color(),
        backgroundColor: random_fill_color(),
        data: bins.totals
      }]
    }
  };
}

/**
 * make doughnut from bins
 * @param {totals: Array[number], labels: Array[string]} bins 
 * @param {string} legend 
 */
function make_doughnut(bins, legend) {
  /* at most eight bins... */
  if (bins.labels.length > 8) {
    var agg = bins.totals.map(function(v,i) {
      return [v, bins.labels[i]];
    });
    /* sort with descending order */
    agg.sort(function(a,b) {
      return a[0] < b[0];
    });
    /* merge some bins */
    bins.totals = new Array(8);
    bins.labels = new Array(8);
    for (var i=0; i<7; i++) {
      bins.totals[i] = agg[i][0];
      bins.labels[i] = agg[i][1];
    }
    bins.totals[7] = agg.slice(7).reduce(function(z,x){return z+x;}, 0);
    bins.labels[7] = 'Other';
  }
  return {
    type: 'doughnut',
    data: {
      labels: bins.labels,
      datasets:[{
        label: legend,
        borderColor: random_border_color(),
        backgroundColor: gen_range_n(bins.totals.length).map(function(i,_) {
          return random_fill_color();
        }),
        data: bins.totals
      }]
    }
  };
}

/**
 * make stacked bar chart object
 * datas[i].length [equals] data_labels.length
 * group_labels.length [equals] datas.length
 * @param {Array[Array[number]]} datas 
 * @param {Array[string]} datas_labels 
 */
function make_stacked_hist(datas, data_labels, group_labels) {
  if (!data_labels) data_labels = gen_labels(datas[0]);
  if (!group_labels) group_labels = gen_labels(datas);
  var result = {
    type: 'bar',
    data: {
      labels: data_labels,
      datasets: [],
    },
    options: {
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }
  };
  datas.forEach(function(v,i) {
    result.data.datasets.push({
      label: group_labels[i],
      data: v,
      borderColor: random_border_color(),
      backgroundColor: random_fill_color(),
    });
  });
  return result;
}

/**
 * make line charts of statistics of data per class
 * @param {Map} crit_points statistics of data (produced by crosn)
 * @param {Array[string]} data_labels 
 */
function make_fake_box_plot(crit_points, data_labels) {
  if (!data_labels) data_labels = gen_labels_n(crit_points.class_size);
  return {
    type: 'line',
    data: {
      labels: data_labels,
      datasets: [
      // {
      //   label: 'maximum',
      //   data: crit_points.maximum,
      //   fill: false,
      //   borderColor: 'red',
      // }, 
      {
        label: 'upper_quantile',
        data: crit_points.upper_quantile,
        fill: false,
        borderColor: random_fill_color(),
      }, {
        label: 'medium',
        data: crit_points.medium,
        fill: false,
        borderColor: random_fill_color(),
      }, {
        label: 'lower_quantile',
        data: crit_points.lower_quantile,
        fill: false,
        borderColor: random_fill_color(),
      }, 
      // {
      //   label: 'minimum',
      //   data: crit_points.minimum,
      //   fill: false,
      //   borderColor: 'purple',
      // }
      ],
    },
  };
}

/**
 * make bins for scatter plot
 * @param {object} data Array of {x: number, y: number}
 */
// function make_scatter_bin_plot(data) {
//   var xs = data.map(function(v){return v.x;});
//   var ys = data.map(function(v){return v.y;});
//   var max_x = maximum(xs);
//   var min_x = minimum(xs);
//   var max_y = maximum(ys);
//   var min_y = minimum(ys);
//   var diff = Math.max(Math.abs(max_x-min_x), Math.abs(max_y-min_y));
//   var num_bins = get_bin_size(diff);
//   var bin_size_x = Math.abs(max_x-min_x) / num_bins;
//   var bin_size_y = Math.abs(max_y-min_y) / num_bins;
//   var bins = gen_arrays_n_2(bin_size_x, bin_size_y);
//   data.forEach(function(v) {
//     var ix = Math.floor((v.x - min_x) / bin_size_x);
//     var iy = Math.floor((v.y - min_y) / bin_size_y);
//     bins[ix][iy] += 1;
//   });

// }

/**
 * read global data sources and produce recommended charts
 */
function get_useful_graphs () {
  // just two plain nulls...maybe display some welcome graph
  if (src_type()) {
    return [make_scatter_chart(generateData(), 'X', 'Y')];
  }

  // just a single number
  var axis = src_type('number');
  if (axis) {
    // sort!
    console.log('just one number');
    var chart_data = get_source(axis.first);
    return [
      make_line_chart(chart_data, null, get_label(axis.first)),
      make_histogram(number_to_id(chart_data), get_label(axis.first)),
      make_doughnut(number_to_id(chart_data), get_label(axis.first)),
      // bar chart!
    ];
  }

  var axis = src_type('number', 'number');
  if (axis) {
    console.log('number number');
    var chart_data_x = get_source('x');
    var chart_data_y = get_source('y');
    var chart_data = zip_scatter_data(chart_data_x, chart_data_y);
    return [
      make_scatter_chart(chart_data, get_label('x'), get_label('y')),
      
    ];
  }

  // just a single string
  var axis = src_type('string');
  if (axis) {
    console.log('just one string');
    var chart_data = get_source(axis.first);
    return [
      make_histogram(str_to_id(chart_data), get_label(axis.first)),
      make_doughnut(str_to_id(chart_data), get_label(axis.first)),
    ];
  }

  var axis = src_type('string', 'string');
  if (axis) {
    console.log('string string');
    var chart_data_x = get_source('x');
    var chart_data_y = get_source('y');
    var cr = cross(chart_data_x, chart_data_y);
    return [
      make_stacked_hist(cr.datas, cr.data_labels, cr.group_labels),

    ];
  }

  var axis = src_type('number', 'string');
  if (axis) {
    console.log('number string');
    var chart_data_n = get_source(axis.first);
    var chart_data_s = get_source(axis.second);
    var crit_points = crosn(chart_data_s, chart_data_n);
    return [
      make_fake_box_plot(crit_points, crit_points.labels),
    ];
  }
  
  // whatever else.. an ERROR!
  console.error("error when getting recommended graph!");
  return null;
}
