/**
 * define graph updaters here
 */

/**
 * generate html for carousel-inner
 * @param {Array[charts]} charts charts to be displayed
 */
function gen_disp_html(charts) {
  var result = "";
  var names = new Array(charts.length);
  for (var i=0; i<names.length; i++)
    names[i] = "generated-canvas-" + i;
  for (var i=0; i<charts.length; i++) {
    result += '<div class="carousel-item';
    if (i == 0) result += ' active';
    result += '"><canvas id="' + names[i] + '"';
    result += '"></canvas></div>';
  }
  return {
    html: result,
    canvas_names: names,
  };
}

function gen_disp_chart(charts, canvas_names) {
  window._charts = [];
  charts.forEach(function(chart,i) {
    var cur_name = "#" + canvas_names[i];
    var context2d = $(cur_name)[0].getContext('2d');
    window._charts.push(new Chart(context2d, chart));
  });
}

function replot() {
  var charts = get_useful_graphs();
  var htmls = gen_disp_html(charts);
  $("#display-inner").html(htmls.html);
  gen_disp_chart(charts, htmls.canvas_names);
}

function on_x_click (attrib_str, url, attrib_ch) {
  if (attrib_str == null) {
    $("#dropdown-x").text("（空）");
    set_source('x', null);
    set_label('x', null);
    replot();
    return;
  }
  else {
    $('#dropdown-x').text(attrib_ch)
    $.ajax({
      url: url,
      type: 'GET',
      data: { axis: 'x', attrib: attrib_str }
    }).done(function (msg) {
      // console.log("ajax success");
      set_source('x', msg.data);
      set_label('x', attrib_ch);
      replot();
    });
  }
}
function on_y_click (attrib_str, url, attrib_ch) {
  if (attrib_str == null) {
    $("#dropdown-y").text("（空）");
    set_source('y', null);
    set_label('y', null);
    replot();
    return;
  }
  else {
    $('#dropdown-y').text(attrib_ch);
    $.ajax({
      url: url,
      type: 'GET',
      data: { axis: 'y', attrib: attrib_str }
    }).done(function (msg) {
      // console.log("ajax success");
      set_source('y', msg.data);
      set_label('y', attrib_ch);
      replot()
    });
  }
}
