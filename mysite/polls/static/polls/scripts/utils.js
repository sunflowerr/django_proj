/**
 * all kinds of utilities
 */

/**
 * declare some global variables
 */
$(function() {
  Chart.defaults.global.defaultFontColor = 'wheat';
  // Chart.defaults.global.defaultFontFamily = 'time';
  Chart.defaults.global.defaultFontSize = 14;
  window._data = {
    source: {
      x: null,
      y: null,
    },
    label: {
      x: null,
      y: null,
    },
  };
  replot();
  
});

function set_source(axis, data) {
  if (axis == 'x')
    window._data.source.x = data;
  else if (axis == 'y')
    window._data.source.y = data;
  else
    console.error("set_source: invalid axis!");
}

function set_label(axis, label) {
  if (axis == 'x')
    window._data.label.x = label;
  else if (axis == 'y')
    window._data.label.y = label;
  else
    console.error("set_label: invalid axis!");
}

function get_source(axis) {
  if (axis == 'x')
    return window._data.source['x'];
  else if (axis == 'y')
    return window._data.source['y'];
  else {
    console.error("get_source: invalid axis name.");
    return null;
  }
}

function get_label(axis) {
  if (axis == 'x')
    return window._data.label.x;
  else if (axis == 'y')
    return window._data.label.y;
  else
    console.error('get_label: invalid axis name.');
    return null;
}

/**
 * check type!
 * @param {target} x 
 */
function type_of(x) {
  if (x == null)
    return 'null';
  else if (Array.isArray(x))
    if (x.length == 0)
      return 'null';
    else
      return typeof(x[0]);
  else
    return typeof(x);
}

function gen_zeros(xs) {
  return xs.map(function(v,i) {return 0;});
}

function gen_arrays(xs) {
  return xs.map(function(v,i) {return [];});
}

function gen_arrays_n_2(n1, n2) {
  var result = new Array(n1);
  result.forEach(function(_,i) {
    result[i] = new Array(n2);
  });
  return result;
}

function gen_zeros_n(n) {
  var result = new Array(n);
  for (var i=0; i<n; i++)
    result[i] = 0;
  return result;
}

/**
 * generate array of 0 ~ n-1
 * @param {number} n 
 */
function gen_range_n(n) {
  var result = new Array(n);
  for (var i=0; i<n; i++)
    result[i] = i;
  return result;
}

/**
 * return array indices as strings
 * @param {Array[]} xs 
 */
function gen_labels(xs) {
  return xs.map(function(v,i) {return String(i+1);});
}

function gen_labels_n(n) {
  var result = new Array(n);
  for (var i=1; i<=n; i++)
    result[i] = String(i);
  return result;
}

/**
 * check if data source types match given types
 * if so, return the order of axises according to given types
 * if not, return null
 * @param {string} type_a desired type a
 * @param {string} type_b desired type b
 */
function src_type(type_a, type_b) {
  if (type_a == null) type_a = 'null';
  if (type_b == null) type_b = 'null';
  // ..so that is no parameter is passed, defaulting to null
  var data = window._data;
  var type_x = type_of(data.source.x);
  var type_y = type_of(data.source.y);
  if (type_x == type_a && type_y == type_b)
    return {first: 'x', second: 'y'};
  else if (type_x == type_b && type_y == type_a)
    return {first: 'y', second: 'x'};
  else
    return null;
}

function color_str(r, g, b, a) {
  if (a == null) a = 1;
  return "rgba(" + r + "," + g + "," + b + "," + a + ")";
}

function maximum(xs) {
  var max = null;
  for (var x of xs)
    if (max == null || x > max)
      max = x;
  return max;
}

function minimum(xs) {
  var min = null;
  for (var x of xs)
    if (min == null || x < min)
      min = x;
  return min;
}

window._fill_color = [
  '#6b84a6',
  '#d5ce6d',
  '#7a9700',
  '#fd742d',
  '#ffa5a9',
  '#b0d3bf',
  '#ff4b44',
  '#fff5a5',
  '#7eab11',
  '#cfc270',
  '#8ea253',
  '#c88770',
  '#b44346',
];
window._border_iter = 0;
window._fill_iter = 0;

function random_border_color() {
  // window._border_iter = (window._border_iter + 1) % window._border_color.length;
  // return window._border_color[window._border_iter];
  return 'rgba(0,0,0,0)';
}

function random_fill_color() {
  window._fill_iter = (window._fill_iter + 1) % window._fill_color.length;
  return window._fill_color[window._fill_iter];
}
