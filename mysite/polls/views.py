from django.shortcuts import render
from django.http import HttpResponse, Http404, JsonResponse
from django.template import loader
from django.core.exceptions import FieldDoesNotExist

from .models import CarInfor


# Create your views here.
def index(request):
  fields = [f.name for f in CarInfor._meta.get_fields()]
  fields.remove('id')
  fields.remove('car_name')
  translate = {
    'price': '价格',
    'register_time': '上牌时间',
    'register_location': '上牌地',
    'mileage': '里程',
    'gear_box': '档位',
    'owned': '转手次数',
    'manufacturer': '制造商',
    'grade': '分级',
    'size_x': '长度 x',
    'size_y': '长度 y',
    'size_z': '长度 z',
    'wheelbase': '轴距',
    'weight': '整备重量',
    'displacement': '排量',
    'cylinder': '气缸',
    'horsepower': '马力',
    'torque': '扭矩',
    'fueltype': '燃料',
    'intake_form': '进气形式',
    'oil_type': '汽油',
    'fuel_supply': '供油方式',
    'emission_standard': '排放标准',
  }
  fields = [{'en': f, 'ch': translate[f]} for f in fields]
  context = {'carinfor_field_list': fields}
  template = loader.get_template('polls/index.html')
  return HttpResponse(template.render(context, request))

def get_data(request):
  attrib = request.GET['attrib']
  # size = request.GET['size']
  size = 50
  try:
    CarInfor._meta.get_field(attrib)
  except FieldDoesNotExist:
    raise Http404('Attrib Does Not Exist!')
  attrib_data = [x[0] for x in CarInfor.objects.values_list(attrib)]
  return JsonResponse({'data': attrib_data})

